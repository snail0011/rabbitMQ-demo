package com.xut.producer;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.xut.common.constant.MqConstant;
import com.xut.common.factory.MqConnectionFactory;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * 生产者
 */
public class ProducerClient {

    public static void basicPush() throws IOException, TimeoutException {
        Connection connection = MqConnectionFactory.getConnection();
        Channel channel = connection.createChannel();
        // 声明队列
        channel.queueDeclare(MqConstant.QUEUE_NAME, true, false, false, null);
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            // 发送消息
            channel.basicPublish("", MqConstant.QUEUE_NAME, null, scanner.nextLine().getBytes("UTF-8"));
        }

    }

    public static void main(String[] args) throws IOException, TimeoutException {
        basicPush();
    }

}
